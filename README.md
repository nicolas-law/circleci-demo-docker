# CircleCI 2.0 Demo Application: Building Docker image [![CircleCI](https://circleci.com/gh/nicolas-law/circleci-demo-docker.svg?style=svg)](https://circleci.com/gh/nicolas-law/circleci-demo-docker) [![MIT Licensed](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/circleci/cci-demo-react/master/LICENSE)

This is an example application showcasing how to build Docker images in CircleCI 2.0.

You can follow along with this project by reading the following doc: https://circleci.com/docs/2.0/building-docker-images
